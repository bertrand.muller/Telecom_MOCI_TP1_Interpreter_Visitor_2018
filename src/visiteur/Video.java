package visiteur;

import java.util.ArrayList;

public class Video extends Media {
    
    /**
     * File name
     */
    public String nomFichier;


    /**
     * Author
     */
    public String auteur;


    /**
     * Constructor
     * @param a Year
     * @param nom Name
     * @param auteur Author
     * @param nomFichier File name
     */
    protected Video(int a, String nom, String auteur, String nomFichier) {
        super(a, nom);
        this.auteur = auteur;
        this.nomFichier = nomFichier;
    }


    /**
     * Method used to get the number of pictures for a Video
     * @param vnp Visitor for the number of pictures
     * @return Number of pictures
     */
    @Override
    public int accept(VisitNbPhotos vnp) {
        return vnp.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Video
     * @param vaa Visitor to get Articles of a given author
     * @return List of Articles
     */
    @Override
    protected ArrayList<Media> accept(VisitArticlesAuteur vaa) {
        return vaa.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Video
     * @param vmv Visitor to get MP4 Videos
     * @return List of Videos
     */
    @Override
    protected ArrayList<Media> accept(VisitMP4Videos vmv) {
        return vmv.visit(this);
    }

}
