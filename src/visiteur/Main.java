package visiteur;

import java.util.Calendar;
import java.util.Date;

public class Main {

    /**
     * Main program
     * @param args Arguments
     */
    public static void main(String[] args) {

        Album al = new Album(2018, "Album Senja");
        Photo p1  = new Photo(2018, "Aurore Boréale", "JMP", "Photo1.jpg");
        Photo p2  = new Photo(2018, "Trek", "JMP", "Photo2.jpg");
        Photo p3  = new Photo(2018, "Fjord", "SM", "Photo3.jpg");
        al.ajouter(p1, p2, p3);

        String res = "Album{als=[Photo{auteur='JMP', annee=2018, nom='Aurore Boréale'}, Photo{auteur='JMP', annee=2018, nom='Trek'}, Photo{auteur='SM', annee=2018, nom='Fjord'}]}";
        assert (al.toString().equals(res)) : "Bug" ;


        // Test to verify the number of pictures
        assert(al.accept(new VisitNbPhotos()) == 3) : "Le nombre de photos n'est pas égal à 3 !";


        // Test to verify the number of pictures after adding a Video and an Article
        Article art = new Article(2018, "Journal", "OKL", "Ceci est un bel article...");
        Video v = new Video(2018, "Video", "MLO", "video.avi");
        al.ajouter(art, v);
        assert(al.accept(new VisitNbPhotos()) == 3) : "Le nombre de photos n'est pas égal à 3 !";


        // Test if an article is present or not in the articles list for a given author
        Article art2 = new Article(2018, "Journal2", "GFR", "Ceci est aussi un bel article...");
        al.ajouter(art2);
        assert(al.accept(new VisitArticlesAuteur("OKL")).contains(art)) : "La liste devrait contenir l'article \"art\" !";
        assert(!al.accept(new VisitArticlesAuteur("OKL")).contains(art2)) : "La liste ne devrait pas contenir l'article \"art2\" !";


        // Test if a video is present or not in the videos list
        Video v2 = new Video(2018, "Video 2", "PSD", "video.mp4");
        al.ajouter(v2);
        assert(al.accept(new VisitMP4Videos()).contains(v2)) : "La liste devrait contenir la vidéo \"v2\" !";
        assert(!al.accept(new VisitMP4Videos()).contains(v)) : "La liste ne devrait pas contenir la vidéo \"v\" !";


        // Test to verify the number of pictures after deleting a Photo
        al.supprimer(p1);
        assert(al.accept(new VisitNbPhotos()) == 2) : "Le nombre de photos n'est pas égal à 2 !";


        // Test to verify the number of pictures after adding two SupportDeComm
        SupportDeCommAdaptateur sdc = new SupportDeCommAdaptateur(2018, "SDC", "sdc.txt", "Ceci est un support de comm...");
        SupportDeCommAdaptateur sdc2 = new SupportDeCommAdaptateur(2018, "SDC2", "sdc2.txt", "Ceci est un support de comm...");
        al.ajouter(sdc, sdc2);
        assert(al.accept(new VisitNbPhotos()) == 4) : "Le nombre de photos n'est pas égal à 4 !";


        // Test to verify if the number of videos is still correct after adding some adapters
        assert(al.accept(new VisitMP4Videos()).size() == 1) : "Le nombre de vidéos devrait être égal à 1 !";


        // Test to verify if the number of articles of a given author is still correct after adding some adapters
        assert(al.accept(new VisitArticlesAuteur("OKL")).size() == 1) : "Le nombre de vidéos devrait être égal à 1 !";

    }

}
