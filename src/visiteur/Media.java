package visiteur;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public abstract class Media {

    protected int annee ;

    protected String nom ;


    protected Media(int a, String nom) {
        this.annee = a;
        this.nom = nom;
    }


    /**
     * Method used to allow a visitor to visit Media subclasses
     * @param vnp Visitor for the number of pictures
     * @return Number of pictures
     */
    protected abstract int accept(VisitNbPhotos vnp);


    /**
     * Method used to allow a visitor to visit Media subclasses
     * @param vaa Visitor for the articles of a given author
     * @return List of articles
     */
    protected abstract ArrayList<Media> accept(VisitArticlesAuteur vaa);


    /**
     * Method used to allow a visitor to visit Media subclasses
     * @param vmv Visitor for the MP4 videos
     * @return List of MP4 videos
     */
    protected abstract ArrayList<Media> accept(VisitMP4Videos vmv);

}
