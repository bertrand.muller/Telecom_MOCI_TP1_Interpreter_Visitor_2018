package visiteur;

import appli.SupportDeComm;

import java.util.ArrayList;

public interface Visiteur {

    public int visit(Album a);

    public int visit(Photo p);

    public int visit(Article a);

    public int visit(Video v);

    public int visit(SupportDeCommAdaptateur sdca);

}
