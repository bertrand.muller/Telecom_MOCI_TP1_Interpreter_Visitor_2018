package visiteur;

import java.util.ArrayList;

public class VisitNbPhotos implements Visiteur {

    /**
     * Method used to visit an Album
     * @param a The Album
     * @return Number of pictures
     */
    @Override
    public int visit(Album a) {

        int total = 0;

        for(Media m : a.als) {
            total += m.accept(this);
        }

        return total;

    }


    /**
     * Method used to visit a Photo
     * @param p The Photo
     * @return Number of pictures
     */
    @Override
    public int visit(Photo p) {
        return 1;
    }


    /**
     * Method used to visit an Article
     * @param a The article
     * @return Number of pictures
     */
    @Override
    public int visit(Article a) {
        return 0;
    }


    /**
     * Method used to visit a Video
     * @param v The Video
     * @return Number of pictures
     */
    @Override
    public int visit(Video v) {
        return 0;
    }


    /**
     * Method used to visit a SupportDeCom
     * @param sdca The SupportDeCom
     * @return Number of pictures
     */
    @Override
    public int visit(SupportDeCommAdaptateur sdca) {
        return 1;
    }

}
