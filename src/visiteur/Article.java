package visiteur;

import java.util.ArrayList;

public class Article extends Media {

    /**
     * Text
     */
    public String texte;


    /**
     * Author
     */
    public String auteur;


    /**
     * Constructor
     * @param d Date
     * @param nom Name
     * @param auteur Author
     * @param texte Text
     */
    public Article(int d, String nom, String auteur, String texte) {
        super(d, nom);
        this.auteur = auteur;
        this.texte = texte;
    }


    /**
     * Method used to get the number of pictures for an Article
     * @param vnp Visitor for the number of pictures
     * @return Number of pictures
     */
    @Override
    public int accept(VisitNbPhotos vnp) {
        return vnp.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Article
     * @param vaa Visitor to get Articles of a given author
     * @return List of Articles
     */
    @Override
    protected ArrayList<Media> accept(VisitArticlesAuteur vaa) {
        return vaa.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Article
     * @param vmv Visitor to get MP4 Videos
     * @return List of Videos
     */
    @Override
    protected ArrayList<Media> accept(VisitMP4Videos vmv) {
        return vmv.visit(this);
    }

}
