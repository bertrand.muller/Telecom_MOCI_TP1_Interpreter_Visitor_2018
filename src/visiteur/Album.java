package visiteur;

import java.util.ArrayList;
import java.util.Iterator;

public class Album extends Media implements Iterable<Media> {

    /**
     * Medias
     */
    protected ArrayList<Media> als;


    /**
     * Constructor
     * @param d Date
     * @param nom Name
     */
    public Album(int d, String nom) {
        super(d, nom);
        this.als = new ArrayList<>(10);
    }


    /**
     * Method used to add medias.
     * @param s Medias to add
     */
    public void ajouter(Media... s) {
        for (Media sc : s)
            this.als .add(sc) ;
    }


    /**
     * Method used to print an instance.
     * @return String with object description
     */
    @Override
    public String toString() {
        return "Album{" +
                "als=" + als +
                '}';
    }


    /**
     * Method used to get an iterator on medias.
     * @return Iterator
     */
    @Override
    public Iterator<Media> iterator() {
        return this.als.iterator();
    }


    /**
     * Method used to allow a visitor to get the number of pictures for the Album
     * @param vnp Visitor for the number of pictures
     * @return Number of pictures
     */
    @Override
    protected int accept(VisitNbPhotos vnp) {
        return vnp.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Album
     * @param vaa Visitor to get Articles of a given author
     * @return List of Articles
     */
    @Override
    protected ArrayList<Media> accept(VisitArticlesAuteur vaa) {
        return vaa.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Album
     * @param vmv Visitor to get MP4 Videos
     * @return List of Videos
     */
    @Override
    protected ArrayList<Media> accept(VisitMP4Videos vmv) {
        return vmv.visit(this);
    }


    /**
     * Method used to delete a media in the Album
     * @param m Media to delete
     */
    public void supprimer(Media m) {
        this.als.remove(m);
    }

}