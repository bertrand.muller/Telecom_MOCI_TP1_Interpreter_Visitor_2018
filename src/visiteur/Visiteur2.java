package visiteur;

import java.util.ArrayList;

public interface Visiteur2 {

    public ArrayList<Media> visit(Album a);

    public ArrayList<Media> visit(Photo p);

    public ArrayList<Media> visit(Article a);

    public ArrayList<Media> visit(Video v);

    public ArrayList<Media> visit(SupportDeCommAdaptateur sdca);

}
