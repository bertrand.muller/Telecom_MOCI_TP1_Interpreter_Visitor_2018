package visiteur;

import java.util.ArrayList;

public class Photo extends Media {

    /**
     * Author
     */
    protected String auteur;


    /**
     * File name
     */
    protected String nomFichier;


    /**
     * Constructor
     * @param d Date
     * @param nom Name
     * @param auteur Author
     * @param nomFichier File name
     */
    public Photo(int d, String nom, String auteur, String nomFichier) {
        super(d, nom);
        this.auteur = auteur;
        this.nomFichier = nomFichier;
    }


    /**
     * Method used to print an instance.
     * @return String with object description
     */
    @Override
    public String toString() {
        return "Photo{" +
                "auteur='" + auteur + '\'' +
                ", annee=" + annee +
                ", nom='" + nom + '\'' +
                '}';
    }


    /**
     * Method used to allow a visitor to get the number of pictures for the Photo
     * @param vnp Visitor for the number of pictures
     * @return Number of pictures
     */
    @Override
    protected int accept(VisitNbPhotos vnp) {
        return vnp.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Photo
     * @param vaa Visitor to get Articles of a given author
     * @return List of Articles
     */
    @Override
    protected ArrayList<Media> accept(VisitArticlesAuteur vaa) {
        return vaa.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the Photo
     * @param vmv Visitor to get MP4 Videos
     * @return List of Videos
     */
    @Override
    protected ArrayList<Media> accept(VisitMP4Videos vmv) {
        return vmv.visit(this);
    }

}
