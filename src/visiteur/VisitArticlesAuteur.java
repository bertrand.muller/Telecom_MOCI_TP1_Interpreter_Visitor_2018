package visiteur;

import appli.SupportDeComm;

import java.util.ArrayList;

public class VisitArticlesAuteur implements Visiteur2 {

    /**
     * Author
     */
    private String auteur;


    /**
     * Constructor
     * @param auteur Author
     */
    public VisitArticlesAuteur(String auteur) {
        this.auteur = auteur;
    }


    /**
     * Method used to visit an Album
     * @param a The Album
     * @return List of articles
     */
    @Override
    public ArrayList<Media> visit(Album a) {

        ArrayList<Media> articles = new ArrayList<>();

        for(Media m : a.als) {
            for(Media m2 : m.accept(this)) {
                articles.add(m2);
            }
        }

        return articles;

    }


    /**
     * Method used to visit a Photo
     * @param p The Photo
     * @return List of articles
     */
    @Override
    public ArrayList<Media> visit(Photo p) {
        return new ArrayList<>();
    }


    /**
     * Method used to visit an Article
     * @param a The Article
     * @return List of articles
     */
    @Override
    public ArrayList<Media> visit(Article a) {

        ArrayList<Media> article = new ArrayList<>();

        if(a.auteur.equals(this.auteur)) {
            article.add(a);
        }

        return article;

    }


    /**
     * Method used to visit a Video
     * @param v The Video
     * @return List of articles
     */
    @Override
    public ArrayList<Media> visit(Video v) {
        return new ArrayList<>();
    }


    /**
     * Method used to visit a SupportDeCom
     * @param sdca The SupportDeCom
     * @return List of articles
     */
    @Override
    public ArrayList<Media> visit(SupportDeCommAdaptateur sdca) {

        ArrayList<Media> article = new ArrayList<>();

        if((sdca.support.getAuteur() != null) && (sdca.support.getAuteur().equals(this.auteur))) {
            article.add(sdca);
        }

        return article;

    }

}
