package visiteur;

import appli.SupportDeComm;

import java.util.ArrayList;

public class VisitMP4Videos implements Visiteur2 {

    /**
     * Method used to visit an Album
     * @param a The Album
     * @return List of MP4 videos
     */
    @Override
    public ArrayList<Media> visit(Album a) {

        ArrayList<Media> videos = new ArrayList<>();

        for(Media m : a.als) {
            for(Media m2 : m.accept(this)) {
                videos.add(m2);
            }
        }

        return videos;

    }


    /**
     * Method used to visit a Photo
     * @param p The Photo
     * @return List of MP4 videos
     */
    @Override
    public ArrayList<Media> visit(Photo p) {
        return new ArrayList<>();
    }


    /**
     * Method used to visit an Article
     * @param a The Article
     * @return List of MP4 videos
     */
    @Override
    public ArrayList<Media> visit(Article a) {
        return new ArrayList<>();
    }


    /**
     * Method used to visit a Video
     * @param v The Video
     * @return List of MP4 videos
     */
    @Override
    public ArrayList<Media> visit(Video v) {

        ArrayList<Media> video = new ArrayList<>();

        if(v.nomFichier.contains(".mp4")) {
            video.add(v);
        }

        return video;

    }


    /**
     * Method used to visit a SupportDeCom
     * @param sdca The SupportDeCom
     * @return List of MP4 videos
     */
    @Override
    public ArrayList<Media> visit(SupportDeCommAdaptateur sdca) {
        return new ArrayList<>();
    }

}
