package visiteur;

import appli.SupportDeComm;

import java.util.ArrayList;

public class SupportDeCommAdaptateur extends Media {


    /**
     * Communication support
     */
    protected SupportDeComm support;


    /**
     * Constructor
     * @param a Year
     * @param nom Name
     */
    protected SupportDeCommAdaptateur(int a, String nom, String nomFichier, String texte) {
        super(a, nom);
        this.support = new SupportDeComm(nom, nomFichier, a, texte);
    }


    /**
     * Method used to allow a visitor to get the number of pictures for the SupportDeCom
     * @param vnp Visitor for the number of pictures
     * @return Number of pictures
     */
    @Override
    protected int accept(VisitNbPhotos vnp) {
        return vnp.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the SupportDeCom
     * @param vaa Visitor to get Articles of a given author
     * @return List of Articles
     */
    @Override
    protected ArrayList<Media> accept(VisitArticlesAuteur vaa) {
        return vaa.visit(this);
    }


    /**
     * Method used to allow a visitor to get the list for the SupportDeCom
     * @param vmv Visitor to get MP4 Videos
     * @return List of Videos
     */
    @Override
    protected ArrayList<Media> accept(VisitMP4Videos vmv) {
        return vmv.visit(this);
    }

}
