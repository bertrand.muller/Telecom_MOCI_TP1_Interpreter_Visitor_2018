package interpreteur;

import java.util.ArrayList;

public class Video extends Media {

    /**
     * File name
     */
    public String nomFichier;


    /**
     * Author
     */
    public String auteur;


    /**
     * Constructor
     * @param a Year
     * @param nom Name
     * @param auteur Author
     * @param nomFichier File name
     */
    protected Video(int a, String nom, String auteur, String nomFichier) {
        super(a, nom);
        this.auteur = auteur;
        this.nomFichier = nomFichier;
    }


    /**
     * Method used to get the number of pictures for a Video.
     * @return Number of pictures
     */
    @Override
    public int getNombreDePhotos() {
        return 0;
    }


    /**
     * Method used to return an empty list (a Video is not an Article)
     * @param auteur Author name
     * @return Empty List
     */
    @Override
    protected ArrayList<Media> getArticlesAuteur(String auteur) {
        return new ArrayList<>();
    }


    /**
     * Method used to get all MP4 videos for an Album
     * @return List of videos
     */
    @Override
    protected ArrayList<Media> getVideosMP4() {

        ArrayList<Media> video = new ArrayList<>();

        if(this.nomFichier.contains(".mp4")) {
            video.add(this);
        }

        return video;

    }

}
