package interpreteur;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public abstract class Media {

    /**
     * Year
     */
    protected int annee ;


    /**
     * Name
     */
    protected String nom ;


    /**
     * Constructor
     * @param a Year
     * @param nom Name
     */
    protected Media(int a, String nom) {
        this.annee = a;
        this.nom = nom;
    }


    /**
     * Method used to count the number of pictures for a media.
     * @return Number of pictures
     */
    protected abstract int getNombreDePhotos();


    /**
     * Method used to get all articles of a given author.
     * @param auteur Author name
     * @return List of articles
     */
    protected abstract ArrayList<Media> getArticlesAuteur(String auteur);


    /**
     * Method used to get all MP4 videos.
     * @return List of videos
     */
    protected abstract ArrayList<Media> getVideosMP4();

}
