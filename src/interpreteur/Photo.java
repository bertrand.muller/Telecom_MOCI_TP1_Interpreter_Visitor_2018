package interpreteur;

import interpreteur.Media;

import java.util.ArrayList;

public class Photo extends Media {

    /**
     * Author
     */
    protected String auteur;


    /**
     * File name
     */
    protected String nomFichier;


    /**
     * Constructor
     * @param d Date
     * @param nom Name
     * @param auteur Author
     * @param nomFichier File name
     */
    public Photo(int d, String nom, String auteur, String nomFichier) {
        super(d, nom);
        this.auteur = auteur;
        this.nomFichier = nomFichier;
    }


    /**
     * Method used to print an instance.
     * @return String with object description
     */
    @Override
    public String toString() {
        return "Photo{" +
                "auteur='" + auteur + '\'' +
                ", annee=" + annee +
                ", nom='" + nom + '\'' +
                '}';
    }


    /**
     * Method used to count the number of pictures for a media.
     * @return Number of pictures
     */
    @Override
    protected int getNombreDePhotos() {
        return 1;
    }


    /**
     * Method used to return an empty list (a Photo is not an Article)
     * @param auteur Author name
     * @return Empty List
     */
    @Override
    protected ArrayList<Media> getArticlesAuteur(String auteur) {
        return new ArrayList<>();
    }


    /**
     * Method used to get all MP4 videos for a Photo
     * @return List of videos
     */
    @Override
    protected ArrayList<Media> getVideosMP4() {
        return new ArrayList<>();
    }

}
