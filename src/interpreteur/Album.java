package interpreteur;

import java.util.ArrayList;
import java.util.Iterator;

public class Album extends Media implements Iterable<Media> {


    /**
     * Medias
     */
    protected ArrayList<Media> als ;


    /**
     * Constructor
     * @param d Date
     * @param nom Name
     */
    public Album(int d, String nom) {
        super(d, nom);
        this.als = new ArrayList<>(10);
    }


    /**
     * Method used to add medias.
     * @param s Medias to add
     */
    public void ajouter(Media... s) {
        for (Media sc : s)
            this.als .add(sc) ;
    }


    /**
     * Method used to print an instance.
     * @return String with object description
     */
    @Override
    public String toString() {
        return "Album{" +
                "als=" + als +
                '}';
    }


    /**
     * Method used to get an iterator on medias.
     * @return Iterator
     */
    @Override
    public Iterator<Media> iterator() {
        return this.als.iterator();
    }


    /**
     * Method used to count the number of pictures for a media.
     * @return Number of pictures
     */
    @Override
    protected int getNombreDePhotos() {

        int total = 0;

        for(Media m : als) {
            total += m.getNombreDePhotos();
        }

        return total;

    }


    /**
     * Method used to get all articles of a given author.
     * @param auteur Author name
     * @return List of articles
     */
    protected ArrayList<Media> getArticlesAuteur(String auteur) {

        ArrayList<Media> articles = new ArrayList<>();

        for(Media m : als) {
            for(Media m2 : m.getArticlesAuteur(auteur)) {
                articles.add(m2);
            }
        }

        return articles;

    }


    /**
     * Method used to get all MP4 videos for an Album
     * @return List of videos
     */
    @Override
    protected ArrayList<Media> getVideosMP4() {

        ArrayList<Media> videos = new ArrayList<>();

        for(Media m : als) {
            for(Media m2 : m.getVideosMP4()) {
                videos.add(m2);
            }
        }

        return videos;

    }


    /**
     * Method used to delete a media in the Album
     * @param m Media to delete
     */
    public void supprimer(Media m) {
        this.als.remove(m);
    }

}