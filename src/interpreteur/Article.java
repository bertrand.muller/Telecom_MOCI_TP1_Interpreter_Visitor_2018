package interpreteur;

import java.util.ArrayList;

public class Article extends Media {

    /**
     * Text
     */
    public String texte;


    /**
     * Author
     */
    public String auteur;


    /**
     * Constructor
     * @param d Date
     * @param nom Name
     * @param auteur Author
     * @param texte Text
     */
    public Article(int d, String nom, String auteur, String texte) {
        super(d, nom);
        this.auteur = auteur;
        this.texte = texte;
    }


    /**
     * Method used to get the number of pictures for an Article
     * @return Number of pictures
     */
    @Override
    public int getNombreDePhotos() {
        return 0;
    }


    /**
     * Method used to get a list with the Article, if it has been written by the specified author.
     * @param auteur Author name
     * @return List with the Article
     */
    @Override
    protected ArrayList<Media> getArticlesAuteur(String auteur) {

        ArrayList<Media> article = new ArrayList<>();

        if(this.auteur.equals(auteur)) {
            article.add(this);
        }

        return article;

    }


    /**
     * Method used to get all MP4 videos for an Album
     * @return List of videos
     */
    @Override
    protected ArrayList<Media> getVideosMP4() {
        return new ArrayList<>();
    }

}
