package interpreteur;

import appli.SupportDeComm;

import java.util.ArrayList;

public class SupportDeCommAdaptateur extends Media {


    /**
     * Communication support
     */
    private SupportDeComm support;


    /**
     * Constructor
     * @param a Year
     * @param nom Name
     */
    protected SupportDeCommAdaptateur(int a, String nom, String nomFichier, String texte) {
        super(a, nom);
        this.support = new SupportDeComm(nom, nomFichier, a, texte);
    }


    /**
     * Method used to get the number of pictures for a SupportDeComm
     * @return Number of pictures
     */
    @Override
    protected int getNombreDePhotos() {
        return 1;
    }


    /**
     * Method used to get a list with the SupportDeComm, if it has been written by the specified author.
     * @param auteur Author name
     * @return List with the SupportDeComm
     */
    @Override
    protected ArrayList<Media> getArticlesAuteur(String auteur) {

        ArrayList<Media> support = new ArrayList<>();

        if((this.support.getAuteur() != null) && (this.support.getAuteur().equals(auteur))) {
            support.add(this);
        }

        return support;

    }


    /**
     * Method used to get all MP4 videos for a SupportDeComm
     * @return List of videos
     */
    @Override
    protected ArrayList<Media> getVideosMP4() {
        return new ArrayList<>();
    }

}
